const fs = require("fs");
function problem2() {

    const filenames = ["uppercase.txt", "lowercasesplit.txt", "sorted.txt"];
    
    //problem2 #q1: Read the given file lipsum.txt
    fs.readFile('./lipsum.txt', 'utf8', (err, data) => {


        //problem2 #q2: Convert the content to uppercase & write to a new file.
        //              Store the name of the new file in filenames.txt
        const UpperCase = data.toUpperCase();
        fs.writeFileSync(filenames[0], UpperCase, { flag: 'w' }, err => {
            console.log(err);
        });
        fs.writeFileSync("filenames.txt",filenames[0]+"\n", { flag: 'w' }, err => {
            console.log(err);
        });


        //problem2 #q3: Read the new file and convert it to lower case. 
        //              Then split the contents into sentences.
        //              Then write it to a new file.
        //              Store the name of the new file in filenames.txt
        fs.readFile("./"+filenames[0], 'utf8', (err, data) => {
            let sentance = "";
            for (let index = 0; index < data.length; index++) {
                if (data[index] !== '.') {
                    sentance += data[index]
                } else {
                    sentance += '.\n'
                    index += 1;
                }
            }
            sentance = sentance.toLowerCase();
            fs.writeFileSync(filenames[1], sentance, { flag: 'w' }, err => {
                console.log(err);
            });
            fs.writeFileSync("filenames.txt",filenames[1]+"\n", { flag: 'a' }, err => {
                console.log(err);
            });


             //problem2 #q4: Read the new files, sort the content, write it out to
             //              a new file. Store the name of the new file in filenames.txt
            fs.readFile(filenames[1], 'utf8', (err, data)=>{
                const words=[];
                let word = "";
                for(let index=0; index<data.length;  index++){
                    if(data[index]!==' ' && data[index]!=='\n'){
                        word+=data[index];
                    }else{
                        words.push(word);
                        word=""
                    }
                }
                
                fs.writeFileSync(filenames[2], words.sort((a,b)=>a<b?-1:a>b?1:0).join(" "), { flag: 'w' }, err => {
                    console.log(err);
                });
                fs.writeFileSync("filenames.txt",filenames[2]+"\n", { flag: 'a' }, err => {
                    console.log(err);
                });

                //problem2 #q5: Read the contents of filenames.txt
                //              and delete all the new files that are 
                //              mentioned in that list simultaneously.
                fs.readFile('./filenames.txt', 'utf8', (err, data) => {
                    data.split("\n").forEach(filename=>{
                        if(filename.length>0){
                            fs.unlinkSync(filename);
                        }
                    });
                })


            })
        });
    });
}

module.exports = problem2