const fs = require("fs")
function problem1() {
    let dir = 'JSONs';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    [...Array(5).keys()].forEach(key => {
        fs.writeFileSync(`./JSONS/filenames${key}.txt`, "", { flag: 'w' }, err => {
            console.log(err);
        });
    })



    fs.readdir("./JSONs", (err, files) => {
        const dirname= "./JSONs/";
        files.forEach(file => {
            fs.unlinkSync(dirname+file);
        });
    });
}

module.exports = problem1;